package filesHandler;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.Writer;
import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.File;


public class FileHandler {

	public static void createTxtFile(String name, String content){
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(System.getProperty("user.dir")+"/arquivos/"+name+".txt"), "utf-8"))) {
			writer.write(content);
	}catch(Exception e){
		System.out.println(e.getMessage());
	   }

		return;
	}
	
	
	public static void getSelectedFile(MusicFile musicPlaying) {
		
		try {
		     JFileChooser chooser = new JFileChooser(System.getProperty("user.dir")+"/arquivos/");
		     chooser.setFileFilter(new FileNameExtensionFilter("TXT", "txt"));
		     int isOpen = chooser.showOpenDialog(null);
		     
		     if (isOpen == JFileChooser.APPROVE_OPTION) {
		    	 musicPlaying.setTextSequence(FileHandler.readTextFile(chooser.getSelectedFile()));
		    	 musicPlaying.setName(FileHandler.getFileName(chooser.getSelectedFile()));
		       
		     }
		   }catch(Exception e){
				System.out.println(e.getMessage());
		   }
	}

	
	public static String getFileName(File file){
		String name = file.getName();
		name = name.substring(0, name.lastIndexOf('.'));
		return name;
	}
	
	public static String readTextFile(File fileToRead){
		Scanner newText = null;
		String text = "";
		try{
		newText = new Scanner(new FileReader(fileToRead));
		String newLine = null;
		
		while(newText.hasNextLine()){
			newLine = newText.nextLine();
			text = text.concat(newLine);
		}
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
		finally{
			newText.close();
		}
		return text;
}

}
