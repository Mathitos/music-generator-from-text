package filesHandler;

import java.io.*;
import javax.sound.midi.*;

public class MusicFile {
	
	//constants
	public static final int VOLUME_DEFAULT = 100;
	public static final int VOLUME_MAX = 127;
	public static final int DURATION = 120;
	public static final int CONTROL = 176;
	public static final int OMNI_ON = 25;
	public static final int POLY_ON = 127;
	public static final int CHANGE_INSTRUMENT = 192;
	public static final int MAX_INSTRUMENTS = 127;
	public static final int MAIN_VOLUME = 7;
	public static final int NOTE_ON = 144;
	public static final int NOTE_OFF = 128;
	public static final int NOTEVELOCITY = 100;
	public static final int MAXOCTAVES = 10;
	public static final int MAXNOTEVALUE = 127;
	public static final int HARPSICHORD = 6;
	public static final int	TUBULAR_BELLS = 14;
	public static final int PAN_FLUTE = 75;
	public static final int CHURCH_ORGAN = 19;
	public static final int PIANO = 0;
	public static final int C = 0;
	public static final int D = 2;
	public static final int E = 4;
	public static final int F = 5;
	public static final int G = 7;
	public static final int A = 9;
	public static final int B = 11;
	public static final int OCTAVE = 12;
	public static final int MAX_NOTE_VALUE = 127;
	
	//end of constants
	
	private Sequence sequence;
	
	private static Sequencer sequencer;

	
	private String text = "";
	private String name = "";

	public MusicFile() {
		
	}
	
	public String getName(){
		return this.name;
	}

	public void setSequencer(){
		try{
			sequencer = MidiSystem.getSequencer();
			sequencer.setSequence(this.sequence);
			sequencer.open();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return;
	}
	public void changeSequencer(){
		try{
			sequencer.setSequence(this.sequence);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return;
	}
	
	public void startSequence(){
		sequencer.start();
		return;
	}
	
	public void stopSequence(){
		sequencer.stop();
		return;
	}
	
	public void setTextSequence(String string){
		this.text = string;
		return;
	}
	
	
	public void setName(String string){
		this.name = string;
		return;
	}

	public void buildSequence(){
		final byte[] b = {(byte)0xF0, 0x7E, 0x7F, 0x09, 0x01, (byte)0xF7};
		final byte[] bt = {0x02, (byte)0x00, 0x00};
		final byte[] empty = {};
		int volume = VOLUME_DEFAULT;
		int octaveAdd = 0;
		Boolean lastCharWasANote = false;
		int instrumentCode = 0;
		long time = 0;
		
		
		try{
			//initialize midi file
			this.sequence = new Sequence(javax.sound.midi.Sequence.PPQ,24);
			Track track = this.sequence.createTrack();
			SysexMessage sm = new SysexMessage();
			sm.setMessage(b, 6);
			MidiEvent midiEvent = new MidiEvent(sm,(long)0);
			track.add(midiEvent); //turn on General MIDI sound set
			MetaMessage metaMessage = new MetaMessage();
			metaMessage.setMessage(0x51 ,bt, 3);
			midiEvent = new MidiEvent(metaMessage,(long)0);
			track.add(midiEvent); //set tempo
			metaMessage = new MetaMessage();
			String TrackName = new String("track 1");
			metaMessage.setMessage(0x03 ,TrackName.getBytes(),TrackName.length());
			midiEvent = new MidiEvent(metaMessage,(long)0);
			track.add(midiEvent); //set track name
			ShortMessage shortMessage = new ShortMessage();
			shortMessage.setMessage(CONTROL, OMNI_ON,0);
			midiEvent = new MidiEvent(shortMessage,(long)0);
			track.add(midiEvent); //set omni on
			shortMessage = new ShortMessage();
			shortMessage.setMessage(CONTROL, POLY_ON,0);
			midiEvent = new MidiEvent(shortMessage,(long)0);
			track.add(midiEvent); //set poly on
			shortMessage = new ShortMessage();
			shortMessage.setMessage(CHANGE_INSTRUMENT, PIANO, 0);
			midiEvent = new MidiEvent(shortMessage,(long)0);
			track.add(midiEvent); //set instrument to Piano(default)
			
			int count = 0;
			int stringSize = this.text.length()-1;
			
			while(count <= stringSize){
				switch(this.text.charAt(count)){
					case 'A':{
						lastCharWasANote = true;
						if(A + octaveAdd > MAX_NOTE_VALUE)
							octaveAdd = 0;
						shortMessage = new ShortMessage();
						shortMessage.setMessage(NOTE_ON, A + octaveAdd, NOTEVELOCITY);
						time += 1;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						shortMessage = new ShortMessage();
						shortMessage.setMessage(NOTE_OFF, A + octaveAdd, NOTEVELOCITY);
						time += DURATION;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						break;
					}
					case 'B':{
						lastCharWasANote = true;
						if(B + octaveAdd > MAX_NOTE_VALUE)
							octaveAdd = 0;
						shortMessage = new ShortMessage();
						shortMessage.setMessage(NOTE_ON, B + octaveAdd, NOTEVELOCITY);
						time += 1;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						shortMessage = new ShortMessage();
						shortMessage.setMessage(NOTE_OFF, B + octaveAdd, NOTEVELOCITY);
						time += DURATION;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						break;
					}
					case 'C':{
						lastCharWasANote = true;
						if(C + octaveAdd > MAX_NOTE_VALUE)
							octaveAdd = 0;
						shortMessage = new ShortMessage();
						shortMessage.setMessage(NOTE_ON,C + octaveAdd, NOTEVELOCITY);
						time += 1;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						shortMessage = new ShortMessage();
						shortMessage.setMessage(NOTE_OFF, C + octaveAdd, NOTEVELOCITY);
						time += DURATION;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						break;
					}
					case 'D':{
						lastCharWasANote = true;
						if(D + octaveAdd > MAX_NOTE_VALUE)
							octaveAdd = 0;
						shortMessage = new ShortMessage();
						shortMessage.setMessage(NOTE_ON, D + octaveAdd, NOTEVELOCITY);
						time += 1;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						shortMessage = new ShortMessage();
						shortMessage.setMessage(NOTE_OFF, D + octaveAdd, NOTEVELOCITY);
						time += DURATION;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						break;
					}
					case 'E':{
						lastCharWasANote = true;
						if(E + octaveAdd > MAX_NOTE_VALUE)
							octaveAdd = 0;
						shortMessage = new ShortMessage();
						shortMessage.setMessage(NOTE_ON, E + octaveAdd, NOTEVELOCITY);
						time += 1;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						shortMessage = new ShortMessage();
						shortMessage.setMessage(NOTE_OFF, E + octaveAdd, NOTEVELOCITY);
						time += DURATION;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						break;
					}
					case 'F':{
						lastCharWasANote = true;
						if(F + octaveAdd > MAX_NOTE_VALUE)
							octaveAdd = 0;
						shortMessage = new ShortMessage();
						shortMessage.setMessage(NOTE_ON, F + octaveAdd, NOTEVELOCITY);
						time += 1;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						shortMessage = new ShortMessage();
						shortMessage.setMessage(NOTE_OFF, F + octaveAdd, NOTEVELOCITY);
						time += DURATION;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						break;
					}
					case 'G':{
						lastCharWasANote = true;
						if(A + octaveAdd > MAX_NOTE_VALUE)
							octaveAdd = 0;
						shortMessage = new ShortMessage();
						shortMessage.setMessage(NOTE_ON, A + octaveAdd, NOTEVELOCITY);
						time += 1;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						shortMessage = new ShortMessage();
						shortMessage.setMessage(NOTE_OFF, A + octaveAdd, NOTEVELOCITY);
						time += DURATION;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						break;
					}
					case ' ':{
						lastCharWasANote = false;
						if(2*volume < VOLUME_MAX)
							volume = volume * 2;
						else
							volume = VOLUME_MAX;
						shortMessage = new ShortMessage();
						shortMessage.setMessage(CONTROL, MAIN_VOLUME, volume);
						time += 1;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						break;
					}
					case '!':{
						lastCharWasANote = false;
						shortMessage = new ShortMessage();
						shortMessage.setMessage(CHANGE_INSTRUMENT, 0, HARPSICHORD);
						time += 1;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						instrumentCode = HARPSICHORD;
						break;
					}
					case 'O':
					case 'o':
					case 'I':
					case 'i':
					case 'U':
					case 'u':{
						lastCharWasANote = false;
						if(1.1*volume < VOLUME_MAX)
							volume = (int)(volume * 1.1);
						else
							volume = VOLUME_MAX;
						shortMessage = new ShortMessage();
						shortMessage.setMessage(CONTROL, MAIN_VOLUME, volume);
						time += 1;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						break;
					}
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
					case '0':{
						lastCharWasANote = false;
						instrumentCode =+ this.text.charAt(count);
						if (instrumentCode > MAX_INSTRUMENTS)
							instrumentCode -= MAX_INSTRUMENTS;
						shortMessage = new ShortMessage();
						shortMessage.setMessage(CHANGE_INSTRUMENT, 0, instrumentCode);
						time += 1;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						break;
					}
					case '?':
					case '.':{
						lastCharWasANote = false;
						octaveAdd += OCTAVE;
						break;
					}
					case '\n':{
						lastCharWasANote = false;
						shortMessage = new ShortMessage();
						shortMessage.setMessage(CHANGE_INSTRUMENT, 0, TUBULAR_BELLS);
						time += 1;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						instrumentCode = TUBULAR_BELLS;
						break;
					}
					case ';':{
						lastCharWasANote = false;
						shortMessage = new ShortMessage();
						shortMessage.setMessage(CHANGE_INSTRUMENT, 0, PAN_FLUTE);
						time += 1;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						instrumentCode = PAN_FLUTE;
						break;
					}
					case ',':{
						lastCharWasANote = false;
						shortMessage = new ShortMessage();
						shortMessage.setMessage(CHANGE_INSTRUMENT, 0, CHURCH_ORGAN);
						time += 1;
						midiEvent = new MidiEvent(shortMessage,time);
						track.add(midiEvent);
						instrumentCode = CHURCH_ORGAN;
						break;
					}
					default:{
						if(lastCharWasANote){
							lastCharWasANote = false;
							switch(this.text.charAt(count-1)){
								case 'A':{
									if(A + octaveAdd > MAX_NOTE_VALUE)
										octaveAdd = 0;
									shortMessage = new ShortMessage();
									shortMessage.setMessage(NOTE_ON, A + octaveAdd, NOTEVELOCITY);
									time += 1;
									midiEvent = new MidiEvent(shortMessage,time);
									track.add(midiEvent);
									shortMessage = new ShortMessage();
									shortMessage.setMessage(NOTE_OFF, A + octaveAdd, NOTEVELOCITY);
									time += DURATION;
									midiEvent = new MidiEvent(shortMessage,time);
									track.add(midiEvent);
									break;
								}
								case 'B':{
									if(B + octaveAdd > MAX_NOTE_VALUE)
										octaveAdd = 0;
									shortMessage = new ShortMessage();
									shortMessage.setMessage(NOTE_ON, B + octaveAdd, NOTEVELOCITY);
									time += 1;
									midiEvent = new MidiEvent(shortMessage,time);
									track.add(midiEvent);
									shortMessage = new ShortMessage();
									shortMessage.setMessage(NOTE_OFF, B + octaveAdd, NOTEVELOCITY);
									time += DURATION;
									midiEvent = new MidiEvent(shortMessage,time);
									track.add(midiEvent);
									break;
								}
								case 'C':{
									if(C + octaveAdd > MAX_NOTE_VALUE)
										octaveAdd = 0;
									shortMessage = new ShortMessage();
									shortMessage.setMessage(NOTE_ON,C + octaveAdd, NOTEVELOCITY);
									time += 1;
									midiEvent = new MidiEvent(shortMessage,time);
									track.add(midiEvent);
									shortMessage = new ShortMessage();
									shortMessage.setMessage(NOTE_OFF, C + octaveAdd, NOTEVELOCITY);
									time += DURATION;
									midiEvent = new MidiEvent(shortMessage,time);
									track.add(midiEvent);
									break;
								}
								case 'D':{
									if(D + octaveAdd > MAX_NOTE_VALUE)
										octaveAdd = 0;
									shortMessage = new ShortMessage();
									shortMessage.setMessage(NOTE_ON, D + octaveAdd, NOTEVELOCITY);
									time += 1;
									midiEvent = new MidiEvent(shortMessage,time);
									track.add(midiEvent);
									shortMessage = new ShortMessage();
									shortMessage.setMessage(NOTE_OFF, D + octaveAdd, NOTEVELOCITY);
									time += DURATION;
									midiEvent = new MidiEvent(shortMessage,time);
									track.add(midiEvent);
									break;
								}
								case 'E':{
									if(E + octaveAdd > MAX_NOTE_VALUE)
										octaveAdd = 0;
									shortMessage = new ShortMessage();
									shortMessage.setMessage(NOTE_ON, E + octaveAdd, NOTEVELOCITY);
									time += 1;
									midiEvent = new MidiEvent(shortMessage,time);
									track.add(midiEvent);
									shortMessage = new ShortMessage();
									shortMessage.setMessage(NOTE_OFF, E + octaveAdd, NOTEVELOCITY);
									time += DURATION;
									midiEvent = new MidiEvent(shortMessage,time);
									track.add(midiEvent);
									break;
								}
								case 'F':{
									if(F + octaveAdd > MAX_NOTE_VALUE)
										octaveAdd = 0;
									shortMessage = new ShortMessage();
									shortMessage.setMessage(NOTE_ON, F + octaveAdd, NOTEVELOCITY);
									time += 1;
									midiEvent = new MidiEvent(shortMessage,time);
									track.add(midiEvent);
									shortMessage = new ShortMessage();
									shortMessage.setMessage(NOTE_OFF, F + octaveAdd, NOTEVELOCITY);
									time += DURATION;
									midiEvent = new MidiEvent(shortMessage,time);
									track.add(midiEvent);
									break;
								}
								case 'G':{
									if(A + octaveAdd > MAX_NOTE_VALUE)
										octaveAdd = 0;
									shortMessage = new ShortMessage();
									shortMessage.setMessage(NOTE_ON, A + octaveAdd, NOTEVELOCITY);
									time += 1;
									midiEvent = new MidiEvent(shortMessage,time);
									track.add(midiEvent);
									shortMessage = new ShortMessage();
									shortMessage.setMessage(NOTE_OFF, A + octaveAdd, NOTEVELOCITY);
									time += DURATION;
									midiEvent = new MidiEvent(shortMessage,time);
									track.add(midiEvent);
									break;
								}
								default:{
									System.out.println("deu algo errado no default da .buildMidi");
									break;
								}
							}
							break;
						}
						else{
							time += DURATION;
							break;
						}

					}
				}
				count++;
			}
			metaMessage = new MetaMessage();
			metaMessage.setMessage(0x2F,empty,0);
			time += 10;
			midiEvent = new MidiEvent(metaMessage,time);
			track.add(midiEvent);
					}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	public void buildMidi(){
		try{
			File f = new File(System.getProperty("user.dir")+"/arquivos/"+this.name+".mid");
			MidiSystem.write(this.sequence,1,f);
			return;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
}
