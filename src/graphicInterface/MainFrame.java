package graphicInterface;


import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import Interface.InterfaceMain;
import java.awt.SystemColor;


public class MainFrame {

	private JFrame frame;


	public MainFrame() {
		initialize();
	}

	
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBackground(Color.YELLOW);
		frame.getContentPane().setForeground(Color.RED);
		frame.getContentPane().setLayout(null);
		
		JLabel Display = new JLabel();
		Display.setOpaque(true);
		Display.setBounds(30, 22, 635, 102);
		frame.getContentPane().add(Display);
		Display.setText("Select a File\r\n");
		Display.setBackground(SystemColor.desktop);
		Display.setForeground(Color.YELLOW);
		Display.setHorizontalAlignment(SwingConstants.CENTER);
		Display.setFont(new Font("Verdana", Font.PLAIN, 29));
		
		JButton PlayButton = new JButton("Play/Stop");
		PlayButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
			}
		});
		PlayButton.setFont(new Font("Verdana", Font.BOLD, 25));
		PlayButton.setBounds(28, 192, 211, 150);
		frame.getContentPane().add(PlayButton);
		
		JButton LoadButton = new JButton("Load txt");
		LoadButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				InterfaceMain.getMusicToPlay();
				frame.remove(PlayButton);
				frame.validate();
				JButton PlayButton = new JButton("Play/Stop");
				PlayButton.addMouseListener(new MouseAdapter() {
					public void mouseClicked(MouseEvent arg0) {
						InterfaceMain.playButton();
					}
				});
				PlayButton.setFont(new Font("Verdana", Font.BOLD, 25));
				PlayButton.setBounds(28, 192, 211, 150);
				frame.getContentPane().add(PlayButton);
				frame.getContentPane().remove(Display);
				frame.validate();
				JLabel Display = new JLabel();
				Display.setText(InterfaceMain.getMusicName());
				Display.setOpaque(true);
				Display.setBounds(30, 22, 635, 102);
				frame.getContentPane().add(Display);
				Display.setBackground(SystemColor.desktop);
				Display.setForeground(Color.YELLOW);
				Display.setHorizontalAlignment(SwingConstants.CENTER);
				Display.setFont(new Font("Verdana", Font.PLAIN, 29));
			}
		});
		LoadButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		LoadButton.setBounds(310, 192, 164, 57);
		frame.getContentPane().add(LoadButton);
		
		JButton CreateButton = new JButton("Create txt");
		CreateButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				InterfaceMain.createTxtButton();
			}
		});
		CreateButton.setBounds(310, 285, 164, 57);
		frame.getContentPane().add(CreateButton);
		
		JButton SaveMidiButton = new JButton("Save Midi File");
		SaveMidiButton.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				InterfaceMain.saveMidiButton();
			}
		});
		SaveMidiButton.setBounds(513, 192, 164, 57);
		frame.getContentPane().add(SaveMidiButton);	
		
		
		
		frame.setAutoRequestFocus(false);
		frame.setBounds(100, 100, 713, 417);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	

}
