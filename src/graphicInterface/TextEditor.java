package graphicInterface;

import javax.swing.JFrame;
import javax.swing.SpringLayout;

import filesHandler.FileHandler;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TextEditor {

	private JFrame frame;
	private JTextField fileName;

	
	public TextEditor() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JLabel lblInsertFileName = new JLabel("Insert File Name:");
		springLayout.putConstraint(SpringLayout.WEST, lblInsertFileName, 40, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, lblInsertFileName, 53, SpringLayout.NORTH, frame.getContentPane());
		frame.getContentPane().add(lblInsertFileName);
		
		fileName = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, lblInsertFileName, 0, SpringLayout.NORTH, fileName);
		springLayout.putConstraint(SpringLayout.EAST, lblInsertFileName, -20, SpringLayout.WEST, fileName);
		springLayout.putConstraint(SpringLayout.NORTH, fileName, 24, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, fileName, 149, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, fileName, 53, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, fileName, -26, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(fileName);
		fileName.setColumns(10);
		
		JLabel lblInsertYourText = new JLabel("Insert Your Text: ");
		springLayout.putConstraint(SpringLayout.NORTH, lblInsertYourText, 26, SpringLayout.SOUTH, lblInsertFileName);
		springLayout.putConstraint(SpringLayout.EAST, lblInsertYourText, 0, SpringLayout.EAST, lblInsertFileName);
		frame.getContentPane().add(lblInsertYourText);
		
		JEditorPane textContent = new JEditorPane();
		springLayout.putConstraint(SpringLayout.NORTH, textContent, 20, SpringLayout.SOUTH, fileName);
		springLayout.putConstraint(SpringLayout.WEST, textContent, 20, SpringLayout.EAST, lblInsertYourText);
		springLayout.putConstraint(SpringLayout.SOUTH, textContent, 142, SpringLayout.SOUTH, fileName);
		springLayout.putConstraint(SpringLayout.EAST, textContent, 279, SpringLayout.EAST, lblInsertYourText);
		frame.getContentPane().add(textContent);
		
		JButton btnSave = new JButton("Save");
		btnSave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				FileHandler.createTxtFile(fileName.getText(), textContent.getText());
				frame.setVisible(false);
				frame.dispose();
			}
		});
		springLayout.putConstraint(SpringLayout.WEST, btnSave, 172, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, btnSave, -10, SpringLayout.SOUTH, frame.getContentPane());
		frame.getContentPane().add(btnSave);
		frame.setVisible(true);
	}
}
