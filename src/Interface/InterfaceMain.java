package Interface;

import filesHandler.*;
import graphicInterface.*;

public class InterfaceMain {
	private static Boolean play = false;
	private static Boolean initialized = false;
	private static MusicFile musicPlaying = new MusicFile();

	public static void getMusicToPlay() {
		FileHandler.getSelectedFile(musicPlaying);
		musicPlaying.buildSequence();
		play = false;
		return;
	}

	public static void saveMidiButton() {
		musicPlaying.buildMidi();
		return;
	}

	@SuppressWarnings("unused")
	public static void createTxtButton() {
		TextEditor app = new TextEditor();
		return;
	}

	public static String getMusicName() {
		return musicPlaying.getName();
	}

	public static void playButton() {
		if (!initialized) {
			musicPlaying.setSequencer();
			initialized = true;
		} else
			musicPlaying.changeSequencer();
		if (!play) {
			musicPlaying.startSequence();
			play = true;
		} else {
			musicPlaying.stopSequence();
			play = false;
		}
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {

		MainFrame app = new MainFrame();

	}

}
